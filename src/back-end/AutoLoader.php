<?php
    spl_autoload_register('autoLoader');

    function autoLoader ($class)
    {
        $rawPath = __DIR__ . '\\' . $class . '.php';
        $path = str_replace('\\', DIRECTORY_SEPARATOR, $rawPath);

        if(file_exists($path)){
            include_once $path;
        }
    }
