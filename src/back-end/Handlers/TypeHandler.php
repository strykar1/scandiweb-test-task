<?php

namespace Handlers;

use Classes;

class TypeHandler{

    private $map;

    public function __construct() {
        $this->map = [
            "Book" => new Classes\Book(),
            "DVD" => new Classes\DVD(),
            "Furniture" => new Classes\Furniture(),
        ];
    }

    public function getObject($values)
    {
        $Product = $this->map[$values->type];
        $Product->set($values);
        return $Product;
    }

}