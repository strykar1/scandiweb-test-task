<?php

namespace Handlers;

use Database;
use PDO;
use stdClass;

class Controller
{

    private $conn;
    private $requestType;
    private $typeClassHandler;

    public function __construct($requestType)
    {
        $this->conn = Database::getConnection();
        $this->requestType = $requestType;
        $this->typeClassHandler = new TypeHandler();
    }

    public function request()
    {

        $response = null;
        $requestBody = json_decode(file_get_contents('php://input'));
        switch ($this->requestType) {
            case 'GET':
                $response = $this->getRequest();
                break;
            case 'POST':
                $response = $this->postRequest($requestBody);
                break;
            case 'DELETE':
                $response = $this->deleteRequest($requestBody);
                break;
            default:
                $response = $this->notSupportedRequest();
                break;
        }
        return $response;
    }

    public function getRequest()
    {
        $sql = "
        SELECT products.id, SKU, name, price, type,
            json_object
            (
                'size', size,
                'weight', weight,
                'length', length, 'width', width, 'height', height
            ) AS attribute
        FROM ((
            products LEFT JOIN dvd ON products.id = dvd.productId)
                    LEFT JOIN book ON products.id = book.productId)
                    LEFT JOIN furniture ON products.id = furniture.productId;";

        $query = $this->conn->query($sql);
        $results = array();

        if ($query->rowCount() != 0) {
            $response = $query->fetchAll();

            foreach ($response as $entry) {
                $Product = $this->typeClassHandler->getObject($entry);
                $results[] = $Product->get();
            }
        } else {
            $results = array("error" => "Products not found!");
        }
        return $results;
    }

    public function postRequest($requestBody)
    {
        $values = $requestBody;
        $values->attribute = json_encode($values->attribute);
        $product = $this->typeClassHandler->getObject($values);
        $sql = $product->createDatabaseEntrySQL();
        $query = $this->conn->prepare($sql);
        $query->execute();

        return new stdClass();
    }

    public function deleteRequest($requestBody)
    {
        $sql = "
        DELETE FROM products
        WHERE id IN (" . implode(", ", $requestBody) . ");
        ";

        $query = $this->conn->prepare($sql);
        $query->execute();

        return new stdClass();
    }

    public function notSupportedRequest()
    {
        return "unsupported request";
    }
}
