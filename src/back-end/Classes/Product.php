<?php

namespace Classes;

use stdClass;

abstract class Product{

    protected $id;
    protected $SKU;
    protected $name;
    protected $price;
    protected $type;
    protected $attribute;

    public function set($values){
        if(isset($values->id)){
            $this->id = $values->id;
        }
        $this->SKU = $values->SKU;
        $this->name = $values->name;
        $this->price = json_decode($values->price);
        $this->type = $values->type;
    }
    public function get(){
        $retObject = array(
            "id" => $this->id,
            "SKU" => $this->SKU,
            "name" => $this->name,
            "price" => $this->price,
            "type" => $this->type,
            "attribute" => clone $this->attribute,
        );
        return $retObject;
    }

    public function createDatabaseEntrySQL(){
        $keys = "";
        $values = "";
        foreach ($this->attribute as $key => $value) {
            $keys .= $key;
            $values .= $value;
        }

        $sql = "INSERT INTO products
        (SKU, name, price, type)
        VALUES
            ('".$this->SKU."', '".$this->name."', ".$this->price.", '".$this->type."');
        INSERT INTO ". strtolower(get_class($this)) ."
                (". $keys .", productId)
                VALUES
                    (". $values .", (SELECT id FROM products WHERE SKU='".$this->SKU."'));";
        return $sql;
    }

}