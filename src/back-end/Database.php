<?php

class Database
{
    private static $conn = null;

    private function __construct()
    {
        $host = 'localhost';
        $dbName =  'id18192432_scandiweb_task_db';
        $user =  'id18192432_scandiweb_task_admin';
        $password = 'Pa\BVyddR4wj{p?%';

        try
        {
            $dbURI = 'mysql:host=' . $host . ';dbname=' . $dbName;
            $conn = new PDO($dbURI, $user, $password);
            $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$conn = $conn;
        }
        catch(PDOException $e)
        {
            echo "Connection to Database failed: " . $e->getMessage();
            exit;
        }
    }

    public static function getConnection()
    {
        if(!self::$conn){
            new Database();
        }
        return self::$conn;
    }
}
