
const BASEURL = "https://juniortest-ahmed-abdelhamid.000webhostapp.com";
const PRODUCTS_API = BASEURL + "/back-end/";

export default async function getAllProducts() {
    return fetch(
        PRODUCTS_API,
        {
            headers: {
                'Content-Type': 'application/json',
                'cache-control': 'no-store',
            },
            method: "GET",
        }
      )
      .then(response => response.json())
}
export function addNewProduct(productData) {
    fetch(
        PRODUCTS_API,
        {
            headers: {
                'Content-Type': 'application/json',
            },
            method: "POST",
            body: JSON.stringify(productData),
        }
      )
      .then(response => response.json())
      .catch(err => console.error(err))
}
export function removeProducts(productIds) {
    fetch(
        PRODUCTS_API + "?method=DELETE",
        {
            headers: {
                'Content-Type': 'application/json',
            },
            method: "POST",
            body: JSON.stringify(productIds),
        }
      )
      .then(response => response.json())
      .catch(err => console.error(err))
}
