import ProductDVDForm from "./forms/ProductDVDForm";
import ProductBookForm from "./forms/ProductBookForm";
import ProductFurnitureForm from "./forms/ProductFurnitureForm";

export const ProductTypeAttributeMap = {
    "DVD": ({attribute}) => <span className="font-size-sm">Size: {attribute.size}MB</span>,
    "Book": ({attribute}) => <span className="font-size-sm">Weight: {attribute.weight}Kg</span>,
    "Furniture": ({attribute}) => <span className="font-size-sm">Dimension: {attribute.height}x{attribute.width}x{attribute.length}</span>,
}

export const ProductTypeFormMap = {
    "DVD": ProductDVDForm,
    "Book": ProductBookForm,
    "Furniture": ProductFurnitureForm,
    "None": () => <div/>,
}

export const ProductTypes = {
    DVD: "DVD",
    BOOK: "Book",
    FURNITURE: "Furniture",
}