import { getIn } from 'formik';
import React from 'react';

function ProductFurnitureForm ({formik}) {


    return (
        <div>
            <div className='row my-4'>
                <label className='col-md-2' >Height (CM)</label>
                <input
                className='col-md-4'
                id='height'
                name='attribute.height'
                type="number"
                value={formik.values.attribute.height}
                onChange={formik.handleChange}
                />
                {(getIn(formik.touched,"attribute.height") && getIn(formik.errors,"attribute.height")) &&
                <div className='col-md-5 text-danger'>{getIn(formik.errors,"attribute.height")}</div>}
            </div>
            <div className='row my-4'>
                <label className='col-md-2' >Width (CM)</label>
                <input
                className='col-md-4'
                id='width'
                name='attribute.width'
                type="number"
                value={formik.values.attribute.width}
                onChange={formik.handleChange}
                />
                {(getIn(formik.touched,"attribute.width") && getIn(formik.errors,"attribute.width")) &&
                <div className='col-md-5 text-danger'>{getIn(formik.errors,"attribute.width")}</div>}
            </div>
            <div className='row my-4'>
                <label className='col-md-2' >Length (CM)</label>
                <input
                className='col-md-4'
                id='length'
                name='attribute.length'
                type="number"
                value={formik.values.attribute.length}
                onChange={formik.handleChange}
                />
                {(getIn(formik.touched,"attribute.length") && getIn(formik.errors,"attribute.length")) &&
                <div className='col-md-5 text-danger'>{getIn(formik.errors,"attribute.length")}</div>}
            </div>
            <p className='row my-4'>Please, provide Dimension</p>
        </div>
    );
};

export default ProductFurnitureForm;