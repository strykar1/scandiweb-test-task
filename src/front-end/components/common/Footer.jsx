import React from 'react';
import '../../scss/Footer.scss';

function Footer () {
    return (
        <footer id="footer" className="mx-auto pb-3 mt-auto">
            <hr/>
            <div className='text-center'>
                <span className='text-muted'>Scandiweb Test assignment</span>
            </div>
        </footer>
    );
};

export default Footer;