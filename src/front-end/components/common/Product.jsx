import React, { useState } from 'react';
import '../../scss/Product.scss';
import { ProductTypeAttributeMap } from '../ProductTypeComponentMap';

function Product ({productData, onSelectHandle}) {
    const [selected] = useState(false);

    const {SKU, name, price, type, attribute} = productData

    const AttributeComponent = ProductTypeAttributeMap[type] || <></>

    return (
        <div className="card h-100 w-100 p-1">
            <input className='delete-checkbox m-2' type="checkbox" value={selected} onChange={onSelectHandle}/>
            <div className='card-body text-center justify-content-evenly'>
                <p className="card-text mw-100 mh-100 font-size-sm">{SKU}</p>
                <p className="card-text mw-100 mh-100 text-wrap font-size-sm" id="card-name-region">{name}</p>
                <p className="card-text mw-100 mh-100 font-size-sm">{price.toFixed(2)} $</p>
                <AttributeComponent attribute={attribute}/>
            </div>
        </div>
    );
};

export default Product;