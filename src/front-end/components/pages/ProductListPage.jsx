import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import getAllProducts, { removeProducts } from '../../api/endpoints';
import Product from '../common/Product';

function ProductListPage({onPressDelete}) {

  const navigate = useNavigate()

  const [itemList, setItemList] = useState([])
  const [selectedItems, setSelectedItems] = useState([])

  const apiGetProducts =() => {
    getAllProducts()
    .then(results => setItemList([...results] || []))
    .catch(err => console.error(err))
  }

  const HandleItemSelect = (key) => {

    // item selected already exists -> remove that item
    if (selectedItems.includes(key)) {
      const correct = [...selectedItems];
      correct.splice(correct.indexOf(key), 1);
      setSelectedItems(correct);

    // item pressed does not exist -> add that item
    } else{
      setSelectedItems([...selectedItems, key]);
    }
  }

  const onClickMassDelete = () => {
    if(selectedItems.length > 0){
      removeProducts(selectedItems);
      setSelectedItems([]);
    }else{
      alert("No products selected");
    }
    apiGetProducts();
  }

  const onClickAdd = () => {
    navigate('/add-product')
  }

  const listItems = itemList.map((item) =>
    <div key={item.id} className='my-1 mx-3 productCard'>
      <Product key={item.id} productData={item} onSelectHandle={() => HandleItemSelect(item.id)}/>
    </div>
  );

  useEffect(() => {
    apiGetProducts();
  }, [])

  return (
    <div>
      {/* header */}
      <div className="header mb-auto py-3">
        <div className='d-flex flex-row justify-content-between align-items-center'>
          <h1>
            Product list
          </h1>
          <div>
            <button id='add-product-btn' className='btn btn-primary' onClick={onClickAdd}>ADD</button>
            <button id='delete-product-btn' className='btn btn-danger  mx-2' onClick={onClickMassDelete}>MASS DELETE</button>
          </div>
        </div>
        <hr/>
      </div>

      {/* item list */}
      <div className='d-flex flex-row flex-wrap'>
        {listItems}
      </div>
    </div>
  );
}

export default ProductListPage;