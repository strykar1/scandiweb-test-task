import { useFormik } from 'formik';
import React from 'react';
import { useNavigate } from 'react-router-dom';
import * as yup from 'yup';
import { addNewProduct } from '../../api/endpoints';
import { ProductTypeFormMap, ProductTypes } from '../ProductTypeComponentMap';

function AddProductPage() {
  const navigate = useNavigate()

  const formik = useFormik({
    initialValues: {
      SKU: '',
      name: '',
      price: '',
      type: '',
      attribute: {
        size: '',
        weight: '',
        length: '',
        width: '',
        height: '',
      },
    },
    validationSchema: yup.object().shape({
      SKU: yup.string().required('SKU required'),
      name: yup.string().required('Name required'),
      price: yup.number().min(0, ()=> "Please, provide the data of indicated type").required('Price required'),
      type: yup.string().required('Type required'),
      attribute: yup.object().when(
        "type",
        {
          is: ProductTypes.FURNITURE,
          then: (schema) => schema.shape({
            length: yup.number().min(0, ()=> "Please, provide the data of indicated type").required("Length required"),
            width: yup.number().min(0, ()=> "Please, provide the data of indicated type").required("Width required"),
            height: yup.number().min(0, ()=> "Please, provide the data of indicated type").required("Height required"),
          })
        })
        .when(
        "type",
        {
          is: ProductTypes.BOOK,
          then: (schema) => schema.shape({
            weight: yup.number().min(0, ()=> "Please, provide the data of indicated type").required("Weight required"),
          })
        })
        .when(
        "type",
        {
          is: ProductTypes.DVD,
          then: (schema) => schema.shape({
            size: yup.number().min(0, ()=> "Please, provide the data of indicated type").required("Size required"),
          })
        }),
      }),
    onSubmit: values => {
      addNewProduct(values)
      formik.resetForm()
      navigate('/')
    },
  })

  const onClickCancel = () => {
    formik.resetForm()
    navigate('/')
  }

  const selectOptions = Object.keys(ProductTypes).map(key =>{
    return (<option key={ProductTypes[key]} value={ProductTypes[key]}>{ProductTypes[key]}</option>)
  })
  const AttributeForm = ProductTypeFormMap[formik.values.type || "None"]

  return (
    <div>
      <form onSubmit={formik.handleSubmit} id="product_form">
        {/* header */}
        <div className="header mb-auto py-3">
          <div className='d-flex flex-row justify-content-between align-items-center'>
            <h1>
              Add Product
            </h1>
            <div>
              <button type="submit" id='save-product-btn' className='btn btn-primary'>Save</button>
              <button type="button" id='cancel-product-btn' className='btn btn-secondary mx-2' onClick={onClickCancel}>Cancel</button>
            </div>
          </div>
          <hr/>
        </div>

      {/* form */}
        <div className='container-fluid px-3'>
          <div className='row my-4'>
            <label className='col-md-2 form-label'>SKU</label>
            <input
              className='col-md-4'
              id='sku'
              name='SKU'
              type="text"
              value={formik.values.SKU}
              onChange={formik.handleChange}
            />{(formik.touched.SKU && formik.errors.SKU) &&
            <div className='col-md-5 text-danger'>{formik.errors.SKU}</div>}
          </div>
          <div className='row my-4'>
            <label className='col-md-2'>Name</label>
            <input
              className='col-md-4'
              id='name'
              name='name'
              type="text"
              value={formik.values.name}
              onChange={formik.handleChange}
            />{(formik.touched.name && formik.errors.name) &&
              <div className='col-md-5 text-danger'>{formik.errors.name}</div>}
          </div>
          <div className='row my-4 '>
            <label className='col-md-2' >Price($)</label>
            <input
              className='col-md-4'
              id='price'
              name='price'
              type="number"
              value={formik.values.price}
              onChange={formik.handleChange}
            />{(formik.touched.price && formik.errors.price) &&
              <div className='col-md-5 text-danger'>{formik.errors.price}</div>}
          </div>
          <div className='row my-4'>
            <label className='col-md-2' >Type Switcher</label>
            <select
              className='col-md-2'
              id="productType"
              name="type"
              value={formik.values.type}
              onChange={formik.handleChange}
            >
            <option value="" disabled>Product type</option>
            {
              selectOptions
            }
            </select>{(formik.touched.type && formik.errors.type) &&
            <div className='col-md-5 text-danger'>{formik.errors.type}</div>}
          </div>
          <div>
            <AttributeForm formik={formik}/>
          </div>
        </div>
      </form>
    </div>
  );
}

export default AddProductPage;